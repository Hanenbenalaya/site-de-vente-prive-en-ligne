import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { Page404Component } from './page404/page404.component';


const routes: Routes = [
  
  {
    path:'home',
    component:HomeComponent
    
  },
  {
    path:'',
    component:LoginComponent
    
  },
  {
    path:'register',
    component:RegisterComponent
    
  },
  {
    path:'admin',
    component:AdminComponent
    
  },
  {path:'**',
  component:Page404Component

  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
